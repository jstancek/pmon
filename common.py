import os
import subprocess
import sys
import traceback

from logging import getLogger, DEBUG, INFO, WARN, CRITICAL, StreamHandler, Formatter

log = None
script_dir = os.path.dirname(os.path.realpath(__file__))

# the indirection here is required because import of global variables creates a copy
# so if variable is imported before it's initialized, we get copy of None
class class_globals():
    def __init__(self, name):
        self.name = name

    def __getattr__(self, name):
        return getattr(globals()[self.name], name)

    def __getitem__(self, key):
        return globals()[self.name][key]

g_log = class_globals("log")

def run_command(cmd, cwddir=None):
    p = subprocess.Popen(cmd, cwd=cwddir, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    out, err = p.communicate()
    out = out.decode('utf-8')
    return p.returncode, out, err

def safe_run_command(cmd, cwddir=None):
    log.debug('%s', cmd)
    retcode, out, err = run_command(cmd, cwddir)
    if retcode != 0:
        log.warn('Command failed: %s, ret_code: %d', cmd, retcode)
        log.warn(out)
        log.warn(err)
        raise Exception(err)
    log.debug('  ^^[OK]')
    return retcode, out, err

def git_fetch_remotes(repo_cachedir, remotes_url):
    log.debug('Fetching %s into %s', str(remotes_url), repo_cachedir)

    if not os.path.exists(repo_cachedir):
        os.makedirs(repo_cachedir, exist_ok = True)
        safe_run_command('git init --bare', cwddir=repo_cachedir)
    
    _, present_remotes, _ = safe_run_command('git remote -v', cwddir=repo_cachedir)

    for remote_url in remotes_url:
        if remote_url in present_remotes:
            log.debug('Already have remote %s', remote_url)
            continue
        else:
            log.debug('Do not have remote %s', remote_url)

        i = 0
        while 'remote%s' % i in present_remotes:
            i = i + 1
        safe_run_command('git remote add remote%s %s' % (i, remote_url), cwddir=repo_cachedir)

    log.debug('Fetching all remotes')
    safe_run_command('git fetch --all', cwddir=repo_cachedir)

def git_get_resolves_for(repo_cachedir, nvr, recent=True):
    since=''
    if recent:
        since='--since="1 week ago"'

    sha = ''
    ret, lines, err = run_command('git log --format="%%H %%s" --all %s | grep %s' % (since, nvr),
        cwddir=repo_cachedir)
    if ret != 0:
        return ''

    for line in lines.split('\n'):
        if not line.strip():
            continue

        if sha:
            log.warn('Multiple shas found for %s: %s', nvr, lines)
        else:
            sha = line.split()[0]

    ret, lines, err = safe_run_command('git show %s | grep "Resolves:"' % (sha), cwddir=repo_cachedir)
    for line in lines.split('\n'):
        line = line.replace('Resolves:', '')
        line = line.replace('rhbz#', '')
        line = line.replace(',', '')
        return line.strip()
    return ''


def setup_logging():
    global log

    log_format = '%(asctime)25.25s - %(levelname)9.9s - %(filename)20.20s' \
                    ' :%(lineno)5s %(funcName)20.20s - %(message)s'
    log = getLogger('pmon')
    handler = StreamHandler(sys.stderr)
    formatter = Formatter(log_format)
    handler.setFormatter(formatter)
    log.addHandler(handler)
    log.debug('Logging on')

def get_stack_trace():
    lines = []
    exc_type, exc_value, exc_traceback = sys.exc_info()
    stack = traceback.format_exception(exc_type, exc_value, exc_traceback)
    for line in stack:
        lines.append(line)
    return lines

def log_exception(exc, level=CRITICAL):
    log.log(level, 'type(exc), exc:')
    log.log(level, str(type(exc)))
    log.log(level, str(exc))

    exc_type, exc_value, exc_traceback = sys.exc_info()
    stack = traceback.format_exception(exc_type, exc_value, exc_traceback)

    log.log(level, 'stack:')
    for line in stack:
        log.log(level, line)

def load_config(config_path):
    global config
    if not config_path:
        config_path = os.path.join(script_dir, 'default.conf')
    config = configparser.ConfigParser()
    if not config.read(config_path):
        sys.stderr.write('Failed to parse %s\n' % (config_path))
        sys.exit(1)
    log.debug('Config loaded')

    return config
