#!/usr/bin/env python3

import common
import koji
import os
import sys
import time

from common import g_log, safe_run_command
from optparse import OptionParser

options = None
data_dir = common.script_dir

def parse_args():
    parser = OptionParser(usage='Usage: %prog [options]')
    parser.add_option('--config', dest='config', help='config file', default='embargo.conf')
    parser.add_option('-v', '--verbose', dest='verbose',
        help='verbose level',
        action='count', default=0)
    (options, args) = parser.parse_args()
    return (options, args)

def init(options):
    if options.verbose > 0:
        g_log.setLevel(common.DEBUG)
        g_log.debug('Debug logging on')

    os.chdir(data_dir)
    g_log.debug('Init done')


def check_resolved_issues(line):
    ret = []
    g_log.debug('Checking issues: %s', line)
    if not line:
        g_log.warning('Line is empty')
        return ret

    bzs = []
    jiras = []
    for issue in line.split():
        if issue.isnumeric():
            bzs.append(issue)
        else:
            jiras.append(issue)

    ret, out, err = safe_run_command("git-kmt-bz-cache %s" % (" ".join(bzs)), cwddir='dist-git-repos')
    ret, out, err = safe_run_command("git-kmt-bz-show -f '{id} {summary}' %s" % (" ".join(bzs)), cwddir='dist-git-repos')
    for line in out.split():
        if 'embargo' in line.lower():
            g_log.warning('Found embargoed issue %s', line)
            ret.append(line)

    ret, out, err = safe_run_command('git-kmt-ji-cache %s' % (" ".join(jiras)), cwddir='dist-git-repos')
    ret, out, err = safe_run_command("git-kmt-ji-show --format='{id} {summary}' %s" % (" ".join(jiras)), cwddir='dist-git-repos')

    for line in out.split():
        if 'embargo' in line.lower():
            g_log.warning('Found embargoed issue %s', line)
            ret.append(line)

    return ret

def check_resolved_issues_for_kernel(nvr):
    g_log.debug('Checking kernel %s', nvr)
    resolved = common.git_get_resolves_for('dist-git-repos', nvr)
    if not resolved:
        g_log.warning('Could not find kernel %s in dist-git-repos', nvr)
        return []
    return check_resolved_issues(resolved)

def check_recent_builds():
    brewinst = koji.ClientSession('https://brewhub.engineering.redhat.com/brewhub')

    now = time.time()
    time_after = now - 3600*24*1

    #opts = {}
    #opts['completedAfter'] = time_after
    #opts['method'] = 'build'
    #opts['state'] = [koji.TASK_STATES['CLOSED'], koji.TASK_STATES['FAILED'], koji.TASK_STATES['CANCELED']]
    #ret = brewinst.listTasks(opts)

    list_of_nvrs_with_embargoed = []

    ret = brewinst.listBuilds(packageID='kernel', completeAfter=time_after)
    for b in ret:
        nvr='%s-%s-%s' % (b['package_name'], b['version'], b['release'])
        ret = check_resolved_issues_for_kernel(nvr)
        if ret:
            list_of_nvrs_with_embargoed.append(nvr)

    list_of_problem_nvrs = []
    for nvr in list_of_nvrs_with_embargoed:
        g_log.debug('Checking tags for %s', nvr)
        tags = brewinst.listTags(build=nvr)
        for tag in tags:
            if not 'nocompose' in tag:
                list_of_problem_nvrs.append(nvr)
                g_log.warning('Found embargoed content in %s, but tags are not nocompose', nvr)

    return list_of_nvrs_with_embargoed

def main():
    common.setup_logging()
    options, args = parse_args()
    init(options)

    common.git_fetch_remotes('dist-git-repos', ['https://pkgs.devel.redhat.com/git/rpms/kernel', 'https://pkgs.devel.redhat.com/git/rpms/kernel-rt'])
    #check_resolved_issues_for_kernel('kernel-5.14.0-385.el9')

    ret = check_recent_builds()

    g_log.debug('Exiting.')

    if ret:
        sys.exit(1)
    sys.exit(0)

if __name__ == '__main__':
    main()

